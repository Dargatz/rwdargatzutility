﻿using System;
using System.IO;
using RWDargatzUtility;
using RWDargatzUtility.FileParsing;
using RWDargatzUtility.Probability;

namespace UtilityTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            var di = new Dice(4, 6);
            var rand = new Random();

            Console.WriteLine(di.diceTest(1000, rand));
            Console.WriteLine(ParsingUtility.getProjectDirectory("RWDargatzUtility"));
            Console.ReadLine();
        }
    }
}
