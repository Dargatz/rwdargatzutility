﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RWDargatzUtility
{
    public class ConnectionGraph
    {
        private bool[,] graph;
        private int[,] weights;
        private int size;
        private List<List<int>> groups;
        private bool updateGroups;

        public ConnectionGraph(int s)
        {
            size = s;
            graph = new bool[size, size];
            weights = new int[size, size];

            for (int i = 0; i < size; i++)
            {
                graph[i, i] = true;
            }

            updateGroups = true;
        }

        private void editGraph()
        {
            updateGroups = true;
        }

        public override string ToString()
        {
            string returnString = "";
            returnString += "Nodes: " + size + '\n';
            List<List<int>> temp = Groups();
            returnString += "Groups: " + temp.Count + '\n';
            int i = 1;
            foreach (List<int> list in temp)
            {
                returnString += "   G" + i + " size: " + list.Count;
                returnString += "      list: ";
                foreach (int j in list)
                {
                    returnString += "" + j + ", ";
                }
                i++;
                returnString += '\n';
            }

            return returnString;
        }

        #region Basics
        public void TwoWayConnection(int index1, int index2, int weight = 1)
        {
            graph[index1, index2] = true;
            graph[index2, index1] = true;
            weights[index1, index2] = weight;
            weights[index2, index1] = weight;
            editGraph();
        }
        public void OneWayConnection(int index1, int index2, int weight = 1)
        {
            graph[index1, index2] = true;
            weights[index1, index2] = weight;
            editGraph();
        }

        private bool connected(int node1, int node2)
        {
            return graph[node1, node2] || graph[node2, node1];
        }

        private bool traversible(int node1, int node2)
        {
            return graph[node1, node2];
        }

        public IEnumerable<int> IterateTwoWayConnected(int node)
        {
            for (int i = 0; i < size; i++)
            {
                if (graph[node, i] && graph[i, node]) yield return i;
            }
        }

        public IEnumerable<int> IterateNodesInGroup(int group)
        {
            foreach (int node in Groups()[group])
            {
                yield return node;
            }
        }

        #endregion

        #region Algorithms

        public bool IsConnected(int node1, int node2)
        {
            bool[] visited = new bool[size];

            return recursiveDepth(0, node1, node2, connected, ref visited) != null;
        }

        public bool IsTraversible(int node1, int node2)
        {
            bool[] visited = new bool[size];

            return recursiveDepth(0, node1, node2, traversible, ref visited) != null;
        }

        public int? Distance(int node1, int node2)
        {
            bool[] visited = new bool[size];

            return recursiveDepth(0, node1, node2, connected, ref visited);
        }

        private int? recursiveDepth(int depth, int currentNode, int targetNode, Func<int, int, bool> check, ref bool[] visited)
        {
            if (currentNode == targetNode) //We have reached the node
            {
                return depth;
            }
            else
            {
                visited[currentNode] = true;
                int min = 1000000;
                int? bestNode = null;
                int? result;

                bool[] newVisited = (bool[])visited.Clone();

                for (int i = 0; i < size; i++)
                {
                    if (check(currentNode, i) && !visited[i])
                    {
                        result = recursiveDepth(depth + weights[currentNode,i], i, targetNode, check, ref newVisited);
                        if (result != null && result < min)
                        {
                            min = (int)result;
                            bestNode = result;
                        }
                    }
                }
                return bestNode;
            }
        }

        public List<List<int>> Groups()
        {
            if (updateGroups)
            {
                bool[] grouped = new bool[size];
                groups = new List<List<int>>();

                while (!areAllTrue(grouped))
                {
                    List<int> nextGroup = new List<int>();
                    for (int i = 0; i < size; i++)
                    {
                        if (!grouped[i])  //Has not been grouped yet
                        {
                            if (nextGroup.Count == 0) //First in group
                            {
                                nextGroup.Add(i);
                                grouped[i] = true;
                            }
                            else
                            {
                                if (IsConnected(nextGroup[0], i)) //Is connected to first in group
                                {
                                    nextGroup.Add(i);
                                    grouped[i] = true;
                                }
                            }
                        }
                    }
                    groups.Add(nextGroup);
                }
                updateGroups = false;
                return groups;
            }
            else
            {
                return groups;
            }
        }

        private bool areAllTrue(bool[] array)
        {
            foreach (bool b in array)
            {
                if (!b) return false;
            }
            return true;
        }

        #endregion
    }

}
