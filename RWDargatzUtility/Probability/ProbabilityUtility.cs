﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RWDargatzUtility.Probability
{
    public static class ProbabilityUtility
    {



    }

    

    public class WeightedRoller
    {
        private int[] Weights;
        private int Total;

        public WeightedRoller(int[] w)
        {
            Weights = w;

            Total = 0;
            foreach(int i in Weights)
            {
                Total += i;
            }
        }

        public double GetPercentageChance(int index)
        {
            return ((double)Weights[index]) / Total;
        }
    }
}
