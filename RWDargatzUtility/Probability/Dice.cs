﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RWDargatzUtility.Probability
{
    public class Dice
    {
        public static Random StaticGenerator;
        public int NumberOfDice { get; }
        public int SizeOfDice { get; }
        public int Offset { get; }

        private const int ResultColumWidth = 8;
        private const int RollsColumWidth = 8;
        private const int PercentColumWidth = 8;
        public Dice(string def)
        {
            NumberOfDice = Int32.Parse(def.Substring(0, def.IndexOf('D')));
            if (def.Contains('+'))
            {
                SizeOfDice = Int32.Parse(def.Substring(def.IndexOf('D') + 1, def.IndexOf('+') - def.IndexOf('D') - 1).Trim());
                Offset = Int32.Parse(def.Substring(def.IndexOf('+') + 1).Trim());
            }
            else if (def.Contains('-'))
            {
                SizeOfDice = Int32.Parse(def.Substring(def.IndexOf('D') + 1, def.IndexOf('-') - def.IndexOf('D') - 1).Trim());
                Offset = Int32.Parse(def.Substring(def.IndexOf('-') + 1));
            }
            else
            {
                SizeOfDice = Int32.Parse(def.Substring(def.IndexOf('D') + 1).Trim());
                Offset = 0;
            }
        }

        public Dice(int dice, int sides, int offset = 0)
        {
            NumberOfDice = dice;
            SizeOfDice = sides;
            Offset = offset;
        }

        #region Rolling
        public int Roll(Random generator = null)
        {
            return roll(generator ?? StaticGenerator);
        }

        private int roll(Random generator)
        {
            int sum = 0;
            for (int i = 0; i < NumberOfDice; i++) sum += generator.Next(1, SizeOfDice + 1);
            return sum + Offset;
        }

        private int[] simulate(int its, Random generater = null)
        {
            var array = new int[this.Maximum - this.Minimum + 1];

            for (int i = 0; i < its; i++)
            {
                array[this.Roll(generater) - this.Minimum]++;
            }

            return array;
        }
        #endregion

        #region Display
        public override string ToString()
        {
            return "" + NumberOfDice + "D" + SizeOfDice + OffsetString();
        }

        private string OffsetString()
        {
            string offsetString;
            if (Offset > 0) offsetString = " + " + Offset;
            else if (Offset < 0) offsetString = " - " + (Offset * -1);
            else offsetString = "";

            return offsetString;
        }

        public string diceTest(int numberOfTests, Random generator = null)
        {
            var rand = new Random();
            var array = simulate(numberOfTests, generator);

            return printTest(numberOfTests, array);
        }

        public string printTest(int numberOfTests, int[] simulated)
        {
            var weighted = new WeightedRoller(simulated);
            string returnVal = "";
            returnVal += "" + numberOfTests + " rolls of " + this + '\n';
            returnVal += "Result".PadLeft(ResultColumWidth) + "Rolls".PadLeft(RollsColumWidth) + "Percent".PadLeft(PercentColumWidth) + '\n';
            for (int i = 0; i < simulated.Length; i++)
            {
                returnVal += ("" + (i + this.Minimum) + ": ").PadLeft(ResultColumWidth) + ("" + simulated[i]).PadLeft(RollsColumWidth) + ("" + (weighted.GetPercentageChance(i) * 100).ToString("N2")).PadLeft(PercentColumWidth) + "%" + '\n';
            }
            return returnVal;
        }

        #endregion

        #region Calculated Properties

        public int Maximum { get { return NumberOfDice * SizeOfDice + Offset; } }
        public int Minimum { get { return NumberOfDice + Offset; } }
        #endregion
    }
}
