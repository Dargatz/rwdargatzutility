﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RWDargatzUtility
{
    [Serializable]
    public class Tracker<T> where T : ITrackable
    {

        private int currentID;
        private Dictionary<int, T> IdDict;
        private Dictionary<string, int> StringRefDict;


        public Tracker()
        {
            IdDict = new Dictionary<int, T>();
            StringRefDict = new Dictionary<string, int>();
            currentID = 0;
        }

        public void Add(T toTrack, string tempRef = null)
        {
            toTrack.Id = currentID;
            addToTracker(toTrack);

            if (tempRef != null) assignRefName(tempRef, toTrack);
        }

        private void addToTracker(T toTrack)
        {
            IdDict.Add(toTrack.Id, toTrack);
            currentID++;
        }

        public void assignRefName(string refName, ITrackable toTrack)
        {
            StringRefDict.Add(refName, toTrack.Id);
        }

        public IEnumerable<ITrackable> getAll
        {
            get
            {
                foreach (KeyValuePair<int, T> pair in IdDict)
                {
                    yield return pair.Value;
                }
            }
        }

        public int getIDByRef(string refString) { return StringRefDict[refString]; }
        public T get(int id) { return IdDict[id]; }
        public T get(string Ref) { return IdDict[StringRefDict[Ref]]; }
        public int TrackedItems { get { return IdDict.Count; } }
    }


    public interface ITrackable
    {
        public int Id { get; set; }
    }
}
