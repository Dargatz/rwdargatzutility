﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RWDargatzUtility.FileParsing
{
    public static class ParsingUtility
    {

        public static string getProjectFile(string ProjectName, string path)
        {
            path = Path.Combine(getProjectDirectory(ProjectName), path);
            Console.WriteLine(path);
            return getFileString(path);
        }

        public static string getFileString(string path)
        {
            StreamReader reader = new StreamReader(path, true);
            string temp = reader.ReadToEnd();
            reader.Close();
            return temp;
        }


        public static string getProjectDirectory(string ProjectName)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                return Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName,ProjectName);
            } else
            {
                return Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
            }
        }

        public static string getFileStringUnitTest(string ProjectName, string path)
        {
            path = Path.Combine(getProjectDirectory(ProjectName), path);
            StreamReader reader = new StreamReader(path, true);
            string temp = reader.ReadToEnd();
            reader.Close();
            return temp;
        }
    }
}
