﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace RWDargatzUtility.FileParsing
{
    /// <summary>
    /// A class for static parsing of my custom file format. 
    /// </summary>
    public static class StaticParser
    {
        public const string labelMarker = "=";
        public const string sectionMarker = "-";
        public const string sectionStart = "START";
        public const string sectionEnd = "END";
        public const char separator = ':';

        //Given a string of format <val>:<val>:.....:<val>
        //Thes functions are for extracting the val at a certain index in the string
        #region Extract Functions

        /// <summary>
        /// Given a string of format <val><delineator><val><delineator>.....<delineator><val>, get the val at index as a string
        /// </summary>
        /// <param name="line">Line to operate on.</param>
        /// <param name="index">Position in the line to return.</param>
        /// <param name="delineator">The character to separate the string by.</param>
        /// <returns></returns>
        public static string extractString(string line, int index, char delineator = separator)
        {
            string[] parts = getDelimitedStrings(line, delineator);
            return parts[index];
        }

        /// <summary>
        /// Extracts the value at index in a delineated line and returns it as an integer.
        /// </summary>
        /// <param name="line">Line to operate on.</param>
        /// <param name="index">Position in the line to return.</param>
        /// <returns></returns>
        public static int extractInt(string line, int index, char delineator = separator)
        {
            return Int32.Parse(extractString(line,index,delineator));
        }


        #endregion

        #region Misc

        private static int[] stringsToInts(string[] strings)
        {
            int[] returnVal = new int[strings.Length];

            for (int i = 0; i < strings.Length; i++)
            {
                returnVal[i] = Int32.Parse(strings[i]);
            }
            return returnVal;
        }

        private static int[,] createIntBlock(string section)
        {
            StringReader ahead = new StringReader(section);
            string aheadLine = ahead.ReadLine();
            //Find number of columns
            int[] tempArray = getInts(aheadLine);
            int width = tempArray.Length;
            //Find number of rows
            int height = 0;
            while (aheadLine != null)
            {
                height++;
                aheadLine = ahead.ReadLine();
            }
            return new int[height, width];
        }

        
        #endregion

        //Functions for counting the lines or sections in a .mrf file that meet a certain criteria.
        #region Counting

        /// <summary>
        /// Returns instances of lines which begin with "<key>=" in the file
        /// </summary>
        /// <param name="file">File to search.</param>
        /// <param name="key">Value of the labels.</param>
        /// <returns></returns>
        private static int countLabeledLines(string file, string key = null)
        {
            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine().Trim();
            int counter = 0;

            while (currentLine != null)
            {
                currentLine = currentLine.Trim();
                if (currentLine.Contains(labelMarker))
                {
                    if(key != null)
                    {
                        if (getStringLabel(currentLine) == key) counter++;
                    } else
                    {
                        counter++;
                    }
                }
                currentLine = reader.ReadLine();
            }
            return counter;
        }
        
        /// <summary>
        /// Counts the instances of sections starting with <key>-START and ends with <key>-END.
        /// </summary>
        /// <param name="file">The file to count sections in.</param>
        /// <param name="key">The section label</param>
        /// <returns></returns>
        public static int countLabeledSections(string file, string key)
        {
            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine();
            int counter = 0;

            while (currentLine != null)
            {
                currentLine = currentLine.Trim();
                if (isSectionStart(currentLine, key))
                {
                    counter++;
                }
                currentLine = reader.ReadLine();
            }
            return counter;
        }

        #endregion

        //Functions for searching a .mrf file to find a line, group of lines, or a sections that meet certain criteria.
        #region Search Functions

        #region Find Functions

        /// <summary>
        /// Finds blocks of integers within a labeled section.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public static int[,] findIntRows(string file, string sectionName)
        {
            string rows = findLabeledSection(file, sectionName);
            if (rows == null || rows.Trim() == "") return null;

            StringReader behind = new StringReader(rows);
            string behindLine = behind.ReadLine();

            int[] tempArray;
            int[,] array = createIntBlock(rows);
            for (int i = 0; i < array.GetLength(0) && behindLine != null; i++)
            {
                tempArray = getInts(behindLine);
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array[i, j] = tempArray[j];
                }
                behindLine = behind.ReadLine();
            }
            return array;
        }

        public static string[] findStringRows(string file, string sectionName)
        {
            string rows = findLabeledSection(file, sectionName);
            if (rows == null || rows.Trim() == "") return null;

            int numberOfRows = findNumberOfRows(rows);

            var returnVal = new string[numberOfRows];

            StringReader behind = new StringReader(rows);
            string behindLine = behind.ReadLine();
            int counter = 0;

            while (behindLine != null)
            {
                if (behindLine != "")
                {
                    returnVal[counter] = behindLine.Trim();
                    counter++;
                }
                behindLine = behind.ReadLine();
            }

            return returnVal;
        }

        private static int findNumberOfRows(string file)
        {
            StringReader ahead = new StringReader(file);
            string aheadLine = ahead.ReadLine();
            int height = 0;
            while (aheadLine != null)
            {
                if (aheadLine != "") height++;
                aheadLine = ahead.ReadLine();
            }
            return height;
        }

        private static string findLabeledLine(string file, string key)
        {
            StringReader reader = new StringReader(file);
            string currentLine = reader.ReadLine();
            key = key.ToUpper();

            while (currentLine != null)
            {
                currentLine = currentLine.Trim();
                if (isLabeledLine(currentLine))
                {
                    if (getStringLabel(currentLine) == key)
                    {
                        return currentLine;
                    }
                }
                currentLine = reader.ReadLine();
            }
            return null;
        }

        /*
         *  finds the line of syntax "key=......" in the file
         *  used by the other single line find functions
         *  4/30/2020 Dargatz
         *  
         */


        public static string[] findLabeledLines(string file, string key = null)
        {
            string[] lines = new string[countLabeledLines(file, key)];
            int lineCounter = 0;
            if (key != null) key = key.ToUpper();
            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine().Trim();

            while (currentLine != null)
            {
                currentLine = currentLine.Trim();
                if (isLabeledLine(currentLine))
                {
                    if(key == null)
                    {
                        lines[lineCounter] = getLabeledString(currentLine);
                        lineCounter++;
                    } else
                    {
                        if (getStringLabel(currentLine) == key)
                        {
                            lines[lineCounter] = getLabeledString(currentLine);
                            lineCounter++;
                        }
                    } 
                }
                currentLine = reader.ReadLine();
            }
            return lines;
        }

        /*
         *  finds the line of syntax "key=<string>" in the file
         */

        public static string findLabeledString(string file, string key)
        {
            string temp = findLabeledLine(file, key);
            if (temp == null) return null;
            return temp.Substring(temp.IndexOf(labelMarker) + 1);
        }


        /*
         *  finds the line of syntax "key=<int>" in the file
         */

        public static int findLabeledInt(string file, string key)
        {
            string temp = findLabeledLine(file, key);
            if (temp == null) return -1;
            return Int32.Parse(temp.Substring(temp.IndexOf(labelMarker) + 1));
        }

        /*
         *  finds the line of syntax "key=<int>" in the file
         */

        public static double findLabeledDouble(string file, string key)
        {
            string temp = findLabeledLine(file, key);
            return Double.Parse(temp.Substring(temp.IndexOf(labelMarker) + 1));
        }

        /*
         *  finds the line of syntax "key=<t/true or f/false> in the file, and returns a bool for it
         */

        public static bool findLabeledBool(string file, string key)
        {
            string temp = findLabeledLine(file, key);
            return getLabeledBool(temp);
        }

        public static bool[] findLabeledBools(string file, string key, int num)
        {
            string[] temp = findLabeledStrings(file, key, num);
            bool[] returnArray = new bool[temp.Length];

            for (int i = 0; i < returnArray.Length; i++)
            {
                if (temp[i] == "T" || temp[i] == "TRUE" || temp[i] == "Y" || temp[i] == "YES") returnArray[i] = true;
                else returnArray[i] = false;
            }

            return returnArray;
        }

        /*
         *  finds the line of syntax "key=<int>:....:<int>" in the file, where there are num ints
         */

        public static int[] findLabeledInts(string file, string key)
        {
            string line = findLabeledLine(file, key);
            line = line.Substring(line.IndexOf(labelMarker) + 1);
            return stringsToInts(getDelimitedStrings(line, separator));
        }

        public static string[] findLabeledStrings(string file, string key, int num)
        {
            string line = findLabeledLine(file, key);
            return getDelimitedStrings(line.Substring(line.IndexOf(labelMarker) + 1), separator);
        }

        /*
         * Gets a section of strings and turns it into a single string
         */

        public static string findStringSection(string file, string key)
        {
            string section = findLabeledSection(file, key);

            StringReader reader = new StringReader(section);
            string currentLine = reader.ReadLine().Trim();

            string returnVal = "";

            while (currentLine != null)
            {
                returnVal += currentLine.Trim() + " ";
                currentLine = reader.ReadLine();
            }
            return returnVal;
        }

        /*
         * finds all sections of syntax and returns them in a string[]
         * <label>=....
         * ....
         * ....
         * END
         * returns everything but the end line
         */

        public static string[] findLabeledSections(string file, string key)
        {
            string[] sections = new string[countLabeledSections(file, key)];
            bool addLine = false; //true when in a section, false when not
            int currentSection = 0;
            string currentSectionString = "";

            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine().Trim();

            while (currentLine != null)
            {
                currentLine = currentLine.Trim();

                if (addLine)
                {
                    if (isSectionEnd(currentLine, key)) //hit the end of the section
                    {
                        addLine = false;
                        sections[currentSection] = currentSectionString;
                        currentSectionString = "";
                        currentSection++;
                    }
                    else //middle of section
                    {
                        currentSectionString += currentLine + "\n";
                    }

                }
                else
                {

                    if (isSectionStart(currentLine, key)) //label line starting section
                    {
                        addLine = true;
                        //currentSectionString += currentLine + "\n";
                    }
                }

                currentLine = reader.ReadLine();
            }
            return sections;
        }


        public static string findLabeledSection(string file, string sectionName)
        {
            string section = "";

            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine();

            while (!isSectionStart(currentLine, sectionName))
            {
                currentLine = reader.ReadLine();
                if (currentLine == null) return null;
            }
            currentLine = reader.ReadLine().Trim();
            while (!isSectionEnd(currentLine, sectionName))
            {
                section += currentLine + "\n";
                currentLine = reader.ReadLine().Trim();
            }

            return section;
        }
        #endregion

        
        #region Has Functions
        public static bool hasLabeledSection(string file, string sectionName)
        {
            StringReader reader = new StringReader(file);

            string currentLine = reader.ReadLine();

            while (currentLine != null)
            {
                if (isSectionStart(currentLine, sectionName)) return true;
                currentLine = reader.ReadLine();
            }
            return false;
        }
        #endregion

        #endregion

        //Functions for operating on a single line from a .mrf file.
        #region Line Functions

        #region Get Functions
        /*
         * Takes a line of labelled integers separated by : and removes whitespace then returns int[].
         */

        public static int[] getLabeledInts(string line, int num)
        {
            string newLine = line.Substring(line.IndexOf(labelMarker) + 1);
            return stringsToInts(getDelimitedStrings(newLine, separator));
        }

        /*
         * Takes a line of unlabelled integers separated by : and removes whitespace then returns int[].
         */

        public static int[] getInts(string line)
        {
            return stringsToInts(getDelimitedStrings(line, separator));
        }

        public static string[] getDelimitedStrings(string line, char delim)
        {
            int count = line.Count(f => f == delim) + 1;

            if (count == 1) return new string[] { line };
            string[] returnArray = new string[count];

            int start = 0;
            int next = line.IndexOf(delim);

            returnArray[0] = line.Substring(start, next);
            start = next;

            for (int i = 1; i < count; i++)
            {
                next = line.IndexOf(delim, start + 1);
                if (next == -1)
                {
                    returnArray[i] = line.Substring(start + 1);
                }
                else
                {
                    returnArray[i] = line.Substring(start + 1, next - start - 1);
                }

                start = next;
            }
            return returnArray;
        }


        public static int[] getDelimitedInts(string line, char delim)
        {
            string[] vals = getDelimitedStrings(line, delim);
            int[] returnVal = new int[vals.Length];
            for (int i = 0; i < vals.Length; i++)
            {
                returnVal[i] = Int32.Parse(vals[i]);
            }
            return returnVal;
        }

        public static string getLabeledString(string line)
        {
            line = line.Trim();
            return line.Substring(line.IndexOf(labelMarker) + 1);
        }

        public static int getLabeledInt(string line)
        {
            return Int32.Parse(line.Substring(line.IndexOf(labelMarker) + 1));
        }

        public static string[] getArray(string line)
        {
            if (line.Contains('=')) line = getLabeledString(line);
            return getDelimitedStrings(line.Trim(), separator);
        }

        public static int getIntLabel(string line)
        {
            string tempLine = line.Trim();
            return Int32.Parse(tempLine.Substring(0, tempLine.IndexOf(labelMarker)));
        }

        public static string getStringLabel(string line)
        {
            string tempLine = line.Trim();
            return tempLine.Substring(0, tempLine.IndexOf(labelMarker)).ToUpper();
        }

        public static bool getLabeledBool(string line)
        {
            line = line.Substring(line.IndexOf(labelMarker) + 1).ToUpper();
            if (line == "T" || line == "TRUE" || line == "Y" || line == "YES") return true;
            return false;
        }
        #endregion

        #region Is Functions
        private static bool isSectionStart(string line, string key)
        {
            if (line == null || line == "") return false;
            line = line.Trim().ToUpper();
            if (line.Contains(sectionMarker) && line.Substring(0, line.IndexOf(sectionMarker)) == key.ToUpper() && line.Substring(line.IndexOf(sectionMarker) + 1) == sectionStart) return true;
            return false;
        }

        private static bool isSectionEnd(string line, string key)
        {
            if (line == null || line == "") return false;
            line = line.Trim().ToUpper();
            if (line.Contains(sectionMarker) && line.Substring(0, line.IndexOf(sectionMarker)) == key.ToUpper() && line.Substring(line.IndexOf(sectionMarker) + 1) == sectionEnd) return true;
            return false;
        }

        private static bool isLabeledLine(string line)
        {
            return line.Contains(labelMarker);
        }
        #endregion

        #endregion
    }
}
