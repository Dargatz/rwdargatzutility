using Microsoft.VisualStudio.TestTools.UnitTesting;
using RWDargatzUtility.FileParsing;
using System;
using System.IO;

namespace UnitTests
{
    [TestClass]
    public class ParsingUnitTests
    {
        public static string file;

        [TestInitialize]
        public void testSetup()
        {
            file = ParsingUtility.getProjectFile("UnitTests", Path.Combine("Parsing", "ParsingData.mrf"));
            //file = ParsingUtility.getFileString(Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName, "UnitTests","Parsing","ParsingData.mrf"));
        }


        [TestMethod]
        public void FileRetrieval()
        {
            Assert.IsTrue(file != null && file.Length != 0);
        }

        [TestMethod]
        public void SectionCounting()
        {
            Assert.IsTrue(StaticParser.countLabeledSections(file, "UNIT") == 2);
            Assert.IsTrue(StaticParser.countLabeledSections(file, "UNITS") == 1);
        }

        [TestMethod]
        public void FindingLabeledLines()
        {
            string countingSection = StaticParser.findLabeledSection(file, "COUNTING");
            string[] ones = StaticParser.findLabeledLines(countingSection, "ONE");
            string[] twos = StaticParser.findLabeledLines(countingSection, "TWO");
            string[] all = StaticParser.findLabeledLines(countingSection);

            Assert.IsTrue(ones.Length == 6);
            Assert.IsTrue(twos.Length == 4);
            Assert.IsTrue(all.Length == 10);
        }

        [TestMethod]
        public void FindLabeledIntTest()
        {
            Assert.IsTrue(StaticParser.findLabeledInt(file, "INTTEST") == 123456);
        }

        [TestMethod]
        public void FindIntsTest()
        {
            string section = StaticParser.findLabeledSection(file, "INTS");

            int[,] kilo = StaticParser.findIntRows(section, "KILOMETERS");
            int[,] miles = StaticParser.findIntRows(section, "MILES");

            Assert.IsTrue(kilo[0, 0] == 85);
            Assert.IsTrue(kilo[3, 3] == 18);
            Assert.IsTrue(kilo[1, 2] == 72);

            int sum = 0;

            for(int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    sum += miles[i, j];
                }
            }
            Assert.IsTrue(sum == 72);
        }

        [TestMethod]
        public void stringsTest()
        {
            string[] strings = StaticParser.findStringRows(file, "STRINGROWS");

            Assert.IsTrue(strings[1] == "My");
            Assert.IsTrue(strings.Length == 5);
        }
    }
}
